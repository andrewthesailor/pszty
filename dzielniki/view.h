#ifndef VIEW_H
#define VIEW_H

#include <QPushButton>
#include <QTextBrowser>
#include <QSpinBox>
#include <QGridLayout>
#include <QScrollArea>
#include <QRect>
#include <QSize>
#include <QVector>
#include <QLabel>
#include <QLineEdit>

#include "enums.h"

class View
{
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // GRAPHIC OBJECTS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    QVector<QPushButton*> buttons_dividers;

    QPushButton *button_startGameWithOneCPU;
    QPushButton *button_startGameWithTwoCPU;
    QPushButton *button_chooseDivider;
    QPushButton *button_chooseRandomDividers;

    QTextBrowser *browser_gameProgressList;
    QTextBrowser *browser_actualNumberToDivide;
    QTextBrowser *browser_chosenDividers;
    QTextBrowser *browser_firstPlayer;
    QTextBrowser *browser_secondPlayer;
    QTextBrowser *browser_nValue;

    QSpinBox *spinBox_chooseDivider;

    QGridLayout *layout;
    QScrollArea *area_areaForButtons;

    QLabel *label_warnings;
    QLabel *label_gameResults;

    QLineEdit *lineEdit_playerNick;

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // SET BUTTONS AREA
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    const int AMOUNT_OF_BUTTONS_IN_A_ROW = 5;
    const int BUTTON_WIDTH = 40;
    const int BUTTON_HEIGHT = 20;

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // SET GEOMETRIES
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    const int MAX_NICK_SIZE = 12;

    const QRect AREA_GEOMETRY = QRect(50, 140, 340, 220);

    const QRect BUTTON_AI_VS_HUMAN_GEOMETRY = QRect(180, 230, 100, 20);
    const QRect BUTTON_AI_VS_AI_GEOMETRY = QRect(360, 230, 100, 20);
    const QRect BUTTON_CHOOSE_DIVIDER_GEOMETRY = QRect(50, 80, 90, 20);
    const QRect BUTTON_CHOOSE_RANDOM_DIVIDER_GEOMETRY = QRect(50, 110, 150, 20);

    const QRect BROWSER_NUMBER_TO_DIVIDE_GEOMETRY = QRect(280, 70, 200, 30);
    const QRect BROWSER_GAME_PROGRESS_GEOMETRY = QRect(400, 140, 200, 220);
    const QRect BROWSER_CHOSEN_DIVIDERS_GEOMETRY = QRect(260, 20, 150, 400);
    const QRect BROWSER_FIRST_PLAYER_GEOMETRY = QRect(50, 20, 150, 30);
    const QRect BROWSER_SECOND_PLAYER_GEOMETRY = QRect(50, 70, 150, 30);
    const QRect BROWSER_N_VALUE_GEOMETRY = QRect(280, 20, 200, 30);

    const QRect LABEL_WARNINGS_GEOMETRY = QRect(50, 410, 200, 20);
    const QRect LABEL_GAME_RESULTS_GEOMETRY = QRect(50, 380, 200, 20);

    const QRect LINE_EDIT_PLAYER_NICK = QRect(260, 100, 120, 20);

    const QRect SPIN_BOX_GEOMETRY = QRect(50, 50, 60, 20);


public:

    View(QVector<QPushButton *> buttons_dividers,
         QPushButton *button_startGameWithOneCPU, QPushButton *button_startGameWithTwoCPU, QPushButton *button_chooseDivider, QPushButton *button_chooseRandomDividers,
         QTextBrowser *browser_gameProgressList, QTextBrowser *browser_actualNumberToDivide, QTextBrowser *browser_chosenDividers, QTextBrowser *browser_firstPlayer, QTextBrowser *browser_secondPlayer, QTextBrowser *browser_nValue,
         QSpinBox *spinBox_chooseDivider,
         QGridLayout *layout,
         QScrollArea * area_areaForButtons,
         QLabel *label_warnings, QLabel *label_gameResults,
         QLineEdit *lineEdit_playerNick);

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // SETTING OBJECTS GEOMETRY
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void prepareGeometries();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // SETTING VISIBLE OBJECTS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void showMenu();
    void showDividerSelection();
    void showGame();
    void hideEverything();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // AREA PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void prepareArea();
    void prepareButtonsInArea(QVector<int>dividers);

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // LINE EDIT PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void prepareLineEdits();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // BUTTON PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void deleteButton(int value);
    void updateButtonsPosition(int hiddenDivider);

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // BROWSER PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void setBrowsersAlligment();
    void updateActualNumber(long long actualNumber);
    void updateListOfChosenDividers(QString divider);
    void updateNValue(long nValue);
    void addToSelectedDividers(int divider);
    void addMovementToTheList(int player, int divider);
    void setActivePlayer(int player);
    void setPlayerNicks(QString firstPlayer, QString secondPlayer);
    void refreshBrowsers();
    QString getPlayerNickFromLineEdit();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // LABEL PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void warn(Warnings warning);
    void showResults(int player);
    void hideResults();
};

#endif // VIEW_H
