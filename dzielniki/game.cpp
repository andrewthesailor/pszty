#include "game.h"

Game::Game(View *view, int amountOfDividers, int minDivider, int maxDivider)
{
    this->view = view;
    this->amountOfDividers = amountOfDividers;
    this->minDivider = minDivider;
    this->maxDivider = maxDivider;
    srand(time(0));

    this->view->hideEverything();
    this->view->prepareGeometries();
    this->view->showMenu();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// MAIN PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void Game::startGame()
{
    pickRandomNValue();
    view->prepareButtonsInArea(dividers);
    setPlayers(gameType);
    view->updateActualNumber(actualValue);
    view->updateNValue(nValue);
    setPlayerNicks();
    changePlayer();
    view->showGame();
}

void Game::resetGame(GameTypes gameType)
{
    dividers.clear();
    actualValue = 1;
    view->refreshBrowsers();
    selectedDividers = 0;
    this->gameType = gameType;
}

void Game::finishGame()
{
    view->showResults(actualPlayer);
    view->showMenu();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// PRE-GAME FUNCTIONS AND PROCEDURES
// -----------------------------------------------------------------------------------------------------------------------------------------------

void Game::tryToAddDivider(int value)
{
    if(dividers.contains(value))
        view->warn(Warnings::dividerAlreadyExists);
    else
        addDivider(value);
}

void Game::addDivider(int value)
{
    dividers.append(value);
    if(actualValue < LLONG_MAX/maxDivider)
        actualValue *= value;
    selectedDividers++;
    view->addToSelectedDividers(value);
    if(selectedDividers == amountOfDividers)
    {
        sortDividers();
        startGame();
    }
}

void Game::fillWithRandomDividers()
{
    while(selectedDividers < amountOfDividers)
    {
        int randomDivider = (double(rand())/RAND_MAX)*(maxDivider-minDivider)+minDivider;
        if(!dividers.contains(randomDivider))
            addDivider(randomDivider);
    }
}

void Game::sortDividers()
{
    bool anyChange = true;

    while(anyChange)
    {
        anyChange = false;
        for(int i = 0; i < (amountOfDividers-1); i++)
        {
            if(dividers.at(i+1) < dividers.at(i))
            {
                anyChange = true;

                int temp = dividers.at(i);
                dividers.replace(i, dividers.at(i+1));
                dividers.replace(i+1, temp);
            }
        }
    }
}

void Game::setPlayers(GameTypes gameType)
{/*
    playersInGame[0] = PlayerTypes::Ai;

    if(gameType == GameTypes::OneCPU)
        playersInGame[1] = PlayerTypes::Human;
    else
        playersInGame[1] = PlayerTypes::Ai;
        */
    playersInGame[0] = PlayerTypes::Human;
    playersInGame[1] = PlayerTypes::Human;

}

void Game::setPlayerNicks()
{
    QString firstPlayer, secondPlayer;

    firstPlayer = AiNicknameBase.at(rand()%AiNicknameBase.size());

    if(gameType == GameTypes::OneCPU)
    {
        secondPlayer = view->getPlayerNickFromLineEdit();
    }
    else
    {
        secondPlayer = AiNicknameBase.at(rand()%AiNicknameBase.size());
    }

    view->setPlayerNicks(firstPlayer, secondPlayer);
}

void Game::setPlayersOrder()
{
    actualPlayer = rand()%2;
}

void Game::pickRandomNValue()
{
    nValue = rand()%int(sqrt(double(actualValue)));
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// IN-GAME FUNCTIONS AND PROCEDURES
// -----------------------------------------------------------------------------------------------------------------------------------------------

void Game::playerMove(int value)
{
    view->deleteButton(value);
    updateSituationForAi(value);
    view->addMovementToTheList(actualPlayer, value);
    actualValue /= value;
    view->updateActualNumber(actualValue);
    if(checkIfSomeoneHasWon())
        finishGame();
    else
        changePlayer();
}

bool Game::checkIfSomeoneHasWon()
{
    if(actualValue < nValue)
        return true;
    return false;
}

void Game::changePlayer()
{
    actualPlayer = (actualPlayer+1)%2;
    view->setActivePlayer(actualPlayer);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// FUNCTIONS AND PROCEDURES FOR HUMAN PLAYER
// -----------------------------------------------------------------------------------------------------------------------------------------------

void Game::tryHumanMove(int divider)
{
    if(playersInGame[actualPlayer] == PlayerTypes::Human)
        playerMove(divider);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// FUNCTIONS AND PROCEDURES FOR AI PLAYER
// -----------------------------------------------------------------------------------------------------------------------------------------------

void Game::updateSituationForAi(int value)
{

}
