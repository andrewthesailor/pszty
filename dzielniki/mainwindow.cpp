#include "mainwindow.h"
#include "ui_mainwindow.h"

// -----------------------------------------------------------------------------------------------------------------------------------------------
// MAIN WINDOW CONSTRUCTOR AND DESTRUCTOR
// -----------------------------------------------------------------------------------------------------------------------------------------------


MainWindow::MainWindow(int treeHeight, int amountOfDividers, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    amountOfDividers = decreaseIfTooManyDividers(amountOfDividers);
    this->amountOfDividers = amountOfDividers;

    prepareArea();

    prepareButtons(amountOfDividers);
    prepareTextBrowsers();
    prepareSpinBox();
    prepareLabels();
    prepareLineEdits();

    createView();
    createGame();


}

MainWindow::~MainWindow()
{
    deleteAllButtons();
    deleteAllTextBrowsers();
    deleteAllSpinBoxes();
    deleteAllLabels();
    deleteLineEdits();
    deleteView();
    deleteArea();
    deleteGame();
    delete ui;
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// ALLOCATE MEMORY AND CONNECT ACTIONS WITH BUTTONS
// -----------------------------------------------------------------------------------------------------------------------------------------------


void MainWindow::prepareButtons(int numberOfButtons)
{
    for(int i = 0; i < numberOfButtons; i++)
    {
        buttons_dividers.append(new QPushButton("", widget_ScrollAreaContent));
        connect(buttons_dividers.last(), SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    }
    button_startGameWithOneCPU = new QPushButton("Human vs. AI", centralWidget());
    button_startGameWithTwoCPU = new QPushButton("AI vs. AI", centralWidget());
    button_chooseRandomDividers = new QPushButton("Fill with random dividers", centralWidget());
    button_chooseDivider = new QPushButton("Add divider", centralWidget());
    connect(button_startGameWithOneCPU, SIGNAL(clicked()), this, SLOT(oneCpuPushButtonClicked()));
    connect(button_startGameWithTwoCPU, SIGNAL(clicked()), this, SLOT(twoCpuPushButtonClicked()));
    connect(button_chooseRandomDividers, SIGNAL(clicked()), this, SLOT(chooseRandomDividersPushButtonClicked()));
    connect(button_chooseDivider, SIGNAL(clicked()), this, SLOT(chooseDividerPushButtonClicked()));
}

void MainWindow::prepareTextBrowsers()
{
    browser_gameProgressList = new QTextBrowser(centralWidget());
    browser_actualNumberToDivide = new QTextBrowser(centralWidget());
    browser_chosenDividers = new QTextBrowser(centralWidget());
    browser_firstPlayer = new QTextBrowser(centralWidget());
    browser_secondPlayer = new QTextBrowser(centralWidget());
    browser_nValue = new QTextBrowser(centralWidget());
}

void MainWindow::prepareSpinBox()
{
    spinBox_chooseDivider = new QSpinBox(centralWidget());
    spinBox_chooseDivider->setMinimum(THE_SMALLEST_POSSIBLE_DIVIDER);
    spinBox_chooseDivider->setMaximum(THE_BIGGEST_POSSIBLE_DIVIDER);
}

void MainWindow::prepareArea()
{
    area_areaForButtons = new QScrollArea(this);
    widget_ScrollAreaContent = new QWidget(this);
    layout = new QGridLayout();

    area_areaForButtons->setWidgetResizable(true);
    layout->addWidget(area_areaForButtons);

    area_areaForButtons->setWidget(widget_ScrollAreaContent);
    layout = new QGridLayout(widget_ScrollAreaContent);
}

void MainWindow::prepareLabels()
{
    label_warnings = new QLabel("", centralWidget());
    label_gameResults = new QLabel("", centralWidget());
}

void MainWindow::prepareLineEdits()
{
    lineEdit_playerNick = new QLineEdit("", centralWidget());
}

void MainWindow::createView()
{
    view = new View(buttons_dividers, button_startGameWithOneCPU, button_startGameWithTwoCPU, button_chooseDivider, button_chooseRandomDividers,
                    browser_gameProgressList, browser_actualNumberToDivide, browser_chosenDividers,
                    browser_firstPlayer, browser_secondPlayer, browser_nValue,
                    spinBox_chooseDivider,
                    layout,
                    area_areaForButtons,
                    label_warnings, label_gameResults,
                    lineEdit_playerNick);
}

void MainWindow::createGame()
{
    game = new Game(view, amountOfDividers, THE_SMALLEST_POSSIBLE_DIVIDER, THE_BIGGEST_POSSIBLE_DIVIDER);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// RELEASE MEMORY
// -----------------------------------------------------------------------------------------------------------------------------------------------

void MainWindow::deleteAllButtons()
{
    while(!buttons_dividers.empty())
    {
        delete buttons_dividers.last();
        buttons_dividers.pop_back();
    }
    delete button_startGameWithOneCPU;
    delete button_startGameWithTwoCPU;
    delete button_chooseRandomDividers;
    delete button_chooseDivider;
}

void MainWindow::deleteAllTextBrowsers()
{
    delete browser_gameProgressList;
    delete browser_actualNumberToDivide;
    delete browser_chosenDividers;
    delete browser_firstPlayer;
    delete browser_secondPlayer;
    delete browser_nValue;
}

void MainWindow::deleteLineEdits()
{
    delete lineEdit_playerNick;
}

void MainWindow::deleteAllSpinBoxes()
{
    delete spinBox_chooseDivider;
}

void MainWindow::deleteAllLabels()
{
    delete label_warnings;
    delete label_gameResults;
}

void MainWindow::deleteArea()
{
    delete layout;
    delete widget_ScrollAreaContent;
    delete area_areaForButtons;
}

void MainWindow::deleteView()
{
    delete view;
}

void MainWindow::deleteGame()
{
    delete game;
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// BUTTON ACTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void MainWindow::pushButtonClicked()
{
    view->warn(Warnings::clear);
    QPushButton *sender = (qobject_cast<QPushButton *>(QObject::sender()));

    game->tryHumanMove(sender->text().toInt());
}

void MainWindow::oneCpuPushButtonClicked()
{
    view->warn(Warnings::clear);
    game->resetGame(GameTypes::OneCPU);
    view->showDividerSelection();
}

void MainWindow::twoCpuPushButtonClicked()
{
    view->warn(Warnings::clear);
    game->resetGame(GameTypes::TwoCPU);
    view->showDividerSelection();
}

void MainWindow::chooseDividerPushButtonClicked()
{
    view->warn(Warnings::clear);
    int divider = spinBox_chooseDivider->value();
    game->tryToAddDivider(divider);

}

void MainWindow::chooseRandomDividersPushButtonClicked()
{
    view->warn(Warnings::clear);
    game->fillWithRandomDividers();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// ADDICTIONAL FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

int MainWindow::decreaseIfTooManyDividers(int amountOfDividers)
{
    if((THE_BIGGEST_POSSIBLE_DIVIDER - THE_SMALLEST_POSSIBLE_DIVIDER + 1) < amountOfDividers)
    {
        view->warn(Warnings::tooManyDividers);
        return (THE_BIGGEST_POSSIBLE_DIVIDER - THE_SMALLEST_POSSIBLE_DIVIDER + 1);
    }
    return amountOfDividers;
}
