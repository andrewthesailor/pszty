#ifndef ARTIFICIALINTELLIGENCE_H
#define ARTIFICIALINTELLIGENCE_H

class ArtificialIntelligence
{
public:
    ArtificialIntelligence(int treeDepth);
    void deleteFromTree(int value);
    void findNextPaths();
};

#endif // ARTIFICIALINTELLIGENCE_H
