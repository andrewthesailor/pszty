#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QPushButton>
#include <QTextBrowser>
#include <QSpinBox>
#include <QLabel>
#include <QWidget>
#include <QScrollArea>
#include <QGridLayout>
#include <QLineEdit>

#include "view.h"
#include "game.h"
#include "enums.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int treeHeight, int amountOfDividers, QWidget *parent = 0);
    ~MainWindow();

// -----------------------------------------------------------------------------------------------------------------------------------------------
// BUTTON ACTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

private slots:
    void pushButtonClicked();
    void oneCpuPushButtonClicked();
    void twoCpuPushButtonClicked();
    void chooseRandomDividersPushButtonClicked();
    void chooseDividerPushButtonClicked();

private:
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // OBJECTS WITH ALLOCATED MEMORY
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    Ui::MainWindow *ui;

    View *view;
    Game *game;

    QVector<QPushButton*> buttons_dividers;

    QPushButton *button_startGameWithOneCPU;
    QPushButton *button_startGameWithTwoCPU;
    QPushButton *button_chooseDivider;
    QPushButton *button_chooseRandomDividers;

    QTextBrowser *browser_gameProgressList;
    QTextBrowser *browser_actualNumberToDivide;
    QTextBrowser *browser_chosenDividers;
    QTextBrowser *browser_firstPlayer;
    QTextBrowser *browser_secondPlayer;
    QTextBrowser *browser_nValue;

    QLineEdit *lineEdit_playerNick;

    QSpinBox *spinBox_chooseDivider;

    QLabel *label_warnings;
    QLabel *label_gameResults;

    QWidget *widget_ScrollAreaContent;

    QScrollArea *area_areaForButtons;

    QGridLayout *layout;

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // VARIABLES
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    int amountOfDividers;

    const int THE_SMALLEST_POSSIBLE_DIVIDER = 2;
    const int THE_BIGGEST_POSSIBLE_DIVIDER = 100;

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // ALLOCATE MEMORY AND CONNECT ACTIONS WITH BUTTONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void prepareButtons(int numberOfButtons);
    void prepareTextBrowsers();
    void prepareSpinBox();
    void prepareArea();
    void prepareLabels();
    void prepareLineEdits();

    void createView();
    void createGame();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // RELEASE MEMORY
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void deleteAllButtons();
    void deleteAllTextBrowsers();
    void deleteLineEdits();
    void deleteAllSpinBoxes();
    void deleteAllLabels();
    void deleteArea();
    void deleteView();
    void deleteGame();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // ADDICTIONAL FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    int decreaseIfTooManyDividers(int amountOfDividers);


};

#endif // MAINWINDOW_H
