#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T20:24:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dzielniki
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    Artificialintelligence.cpp \
    view.cpp \
    game.cpp

HEADERS  += mainwindow.h \
    Artificialintelligence.h \
    view.h \
    game.h \
    enums.h

FORMS    += mainwindow.ui
