#ifndef GAME_H
#define GAME_H

#include "Artificialintelligence.h"
#include "view.h"
#include "enums.h"

#include <QVector>
#include <cstdlib>
#include <ctime>
#include <climits>

class Game
{
    long int nValue;
    long long int actualValue;
    int amountOfDividers;
    int selectedDividers;

    int minDivider;
    int maxDivider;

    QVector<QString> AiNicknameBase = {"Andrzej", "Beata", "Krzysztof", "Krawczyk"};

    QVector<int> dividers;

    int actualPlayer;
    PlayerTypes playersInGame[2];
    GameTypes gameType;

    ArtificialIntelligence *ai_1;
    ArtificialIntelligence *ai_2;

    View *view;


public:
    Game(View *view, int amountOfDividers, int minDivider, int maxDivider);

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // MAIN PROCEDURES AND FUNCTIONS
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void startGame();
    void resetGame(GameTypes gameType);
    void finishGame();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // PRE-GAME FUNCTIONS AND PROCEDURES
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void tryToAddDivider(int value);
    void addDivider(int value);
    void fillWithRandomDividers();
    void sortDividers();

    void setPlayers(GameTypes gameType);
    void setPlayerNicks();
    void setPlayersOrder();
    void pickRandomNValue();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // IN-GAME FUNCTIONS AND PROCEDURES
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void playerMove(int value);
    bool checkIfSomeoneHasWon();
    void changePlayer();

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // FUNCTIONS AND PROCEDURES FOR HUMAN PLAYER
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void tryHumanMove(int divider);

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // FUNCTIONS AND PROCEDURES FOR AI PLAYER
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    void updateSituationForAi(int value);
};

#endif // GAME_H
