#include "view.h"

View::View(QVector<QPushButton *> buttons_dividers,
           QPushButton *button_startGameWithOneCPU, QPushButton *button_startGameWithTwoCPU, QPushButton *button_chooseDivider, QPushButton *button_chooseRandomDividers,
           QTextBrowser *browser_gameProgressList, QTextBrowser *browser_actualNumberToDivide, QTextBrowser *browser_chosenDividers,
           QTextBrowser *browser_firstPlayer, QTextBrowser *browser_secondPlayer, QTextBrowser *browser_nValue,
           QSpinBox *spinBox_chooseDivider,
           QGridLayout *layout,
           QScrollArea *area_areaForButtons,
           QLabel *label_warnings,
           QLabel *label_gameResults, QLineEdit *lineEdit_playerNick)
{
    this->buttons_dividers = buttons_dividers;
    this->button_startGameWithOneCPU = button_startGameWithOneCPU;
    this->button_startGameWithTwoCPU = button_startGameWithTwoCPU;
    this->button_chooseDivider = button_chooseDivider;
    this->button_chooseRandomDividers = button_chooseRandomDividers;
    this->browser_gameProgressList = browser_gameProgressList;
    this->browser_actualNumberToDivide = browser_actualNumberToDivide;
    this->browser_chosenDividers = browser_chosenDividers;
    this->browser_firstPlayer = browser_firstPlayer;
    this->browser_secondPlayer = browser_secondPlayer;
    this->browser_nValue = browser_nValue;
    this->spinBox_chooseDivider = spinBox_chooseDivider;
    this->layout = layout;
    this->area_areaForButtons = area_areaForButtons;
    this->label_warnings = label_warnings;
    this->label_gameResults = label_gameResults;
    this->lineEdit_playerNick = lineEdit_playerNick;

    setBrowsersAlligment();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// SETTING OBJECTS GEOMETRY
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::prepareGeometries()
{
    button_startGameWithOneCPU->setGeometry(BUTTON_AI_VS_HUMAN_GEOMETRY);
    button_startGameWithTwoCPU->setGeometry(BUTTON_AI_VS_AI_GEOMETRY);
    button_chooseDivider->setGeometry(BUTTON_CHOOSE_DIVIDER_GEOMETRY);
    button_chooseRandomDividers->setGeometry(BUTTON_CHOOSE_RANDOM_DIVIDER_GEOMETRY);

    browser_actualNumberToDivide->setGeometry(BROWSER_NUMBER_TO_DIVIDE_GEOMETRY);
    browser_chosenDividers->setGeometry(BROWSER_CHOSEN_DIVIDERS_GEOMETRY);
    browser_gameProgressList->setGeometry(BROWSER_GAME_PROGRESS_GEOMETRY);
    browser_firstPlayer->setGeometry(BROWSER_FIRST_PLAYER_GEOMETRY);
    browser_secondPlayer->setGeometry(BROWSER_SECOND_PLAYER_GEOMETRY);
    browser_nValue->setGeometry(BROWSER_N_VALUE_GEOMETRY);

    label_warnings->setGeometry(LABEL_WARNINGS_GEOMETRY);
    label_gameResults->setGeometry(LABEL_GAME_RESULTS_GEOMETRY);

    lineEdit_playerNick->setGeometry(LINE_EDIT_PLAYER_NICK);

    spinBox_chooseDivider->setGeometry(SPIN_BOX_GEOMETRY);

    area_areaForButtons->setGeometry(AREA_GEOMETRY);

    for(int i = 0; i < buttons_dividers.size(); i++)
    {
        buttons_dividers.at(i)->setMaximumHeight(BUTTON_HEIGHT);
        buttons_dividers.at(i)->setMinimumHeight(BUTTON_HEIGHT);
        buttons_dividers.at(i)->setMaximumWidth(BUTTON_WIDTH);
        buttons_dividers.at(i)->setMinimumWidth(BUTTON_WIDTH);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// SETTING VISIBLE OBJECTS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::showMenu()
{
    hideEverything();
    button_startGameWithOneCPU->show();
    button_startGameWithTwoCPU->show();
    lineEdit_playerNick->show();
    label_gameResults->show();
}

void View::showDividerSelection()
{
    hideEverything();
    button_chooseDivider->show();
    button_chooseRandomDividers->show();
    browser_chosenDividers->show();
    spinBox_chooseDivider->show();
}

void View::showGame()
{
    hideEverything();
    browser_actualNumberToDivide->show();
    browser_gameProgressList->show();
    browser_firstPlayer->show();
    browser_secondPlayer->show();
    browser_nValue->show();
    area_areaForButtons->show();

    for(int i = 0; i < buttons_dividers.size(); i++)
    {
        buttons_dividers.at(i)->show();
    }
}

void View::hideEverything()
{
    button_startGameWithOneCPU->hide();
    button_startGameWithTwoCPU->hide();
    button_chooseDivider->hide();
    button_chooseRandomDividers->hide();
    browser_chosenDividers->hide();
    browser_firstPlayer->hide();
    browser_secondPlayer->hide();
    browser_nValue->hide();
    browser_actualNumberToDivide->hide();
    browser_gameProgressList->hide();
    area_areaForButtons->hide();
    spinBox_chooseDivider->hide();

    lineEdit_playerNick->hide();

    label_gameResults->hide();

    for(int i = 0; i < buttons_dividers.size(); i++)
    {
        buttons_dividers.at(i)->hide();
    }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// AREA PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::prepareArea()
{
    area_areaForButtons->setGeometry(AREA_GEOMETRY);
}

void View::prepareButtonsInArea(QVector<int> dividers)
{
    for(int i = 0; i < buttons_dividers.size(); i++)
    {
        buttons_dividers.at(i)->setText(QString::number(dividers.at(i)));
        buttons_dividers.at(i)->show();
        layout->addWidget(buttons_dividers.at(i), i/AMOUNT_OF_BUTTONS_IN_A_ROW, i%AMOUNT_OF_BUTTONS_IN_A_ROW);
    }

}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// LINE EDIT PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::prepareLineEdits()
{
    lineEdit_playerNick->setMaxLength(MAX_NICK_SIZE);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// BUTTON PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------


void View::deleteButton(int value)
{
    int whichPushButton = 0;

    while(!(buttons_dividers.at(whichPushButton)->text() == QString::number(value)))
        whichPushButton++;

    buttons_dividers.at(whichPushButton)->hide();
    updateButtonsPosition(value);
}

void View::updateButtonsPosition(int hiddenDivider)
{
    int actualIndexInLayout = 0;
    int actualIndexInVector = 0;

    while(!(buttons_dividers.at(actualIndexInVector)->text() == QString::number(hiddenDivider)))
    {
        if(!buttons_dividers.at(actualIndexInVector)->isHidden())
            actualIndexInLayout++;
        actualIndexInVector++;
    }

    QLayoutItem *item = layout->itemAtPosition(actualIndexInLayout/AMOUNT_OF_BUTTONS_IN_A_ROW, actualIndexInLayout%AMOUNT_OF_BUTTONS_IN_A_ROW);
    layout->removeWidget(dynamic_cast<QWidgetItem*>(item)->widget());

    actualIndexInLayout++;
    actualIndexInVector++;

    while(actualIndexInVector < buttons_dividers.size())
    {
        if(!buttons_dividers.at(actualIndexInVector)->isHidden())
        {
            QLayoutItem *item = layout->itemAtPosition(actualIndexInLayout/AMOUNT_OF_BUTTONS_IN_A_ROW, actualIndexInLayout%AMOUNT_OF_BUTTONS_IN_A_ROW);
            QWidget *widget = dynamic_cast<QWidgetItem*>(item)->widget();
            layout->removeWidget(widget);
            layout->addWidget(widget, (actualIndexInLayout-1)/AMOUNT_OF_BUTTONS_IN_A_ROW, (actualIndexInLayout-1)%AMOUNT_OF_BUTTONS_IN_A_ROW);
            actualIndexInLayout++;
        }
        actualIndexInVector++;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// BROWSER PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::setBrowsersAlligment()
{
    browser_firstPlayer->setAlignment(Qt::AlignCenter);
    browser_secondPlayer->setAlignment(Qt::AlignCenter);
    browser_nValue->setAlignment(Qt::AlignCenter);
    browser_actualNumberToDivide->setAlignment(Qt::AlignCenter);
    browser_chosenDividers->setAlignment(Qt::AlignLeft);
    browser_gameProgressList->setAlignment(Qt::AlignLeft);
}

void View::updateActualNumber(long long int actualNumber)
{
    browser_actualNumberToDivide->setText(QString::number(actualNumber));
}

void View::updateListOfChosenDividers(QString divider)
{
    browser_chosenDividers->append(divider);
}

void View::updateNValue(long int nValue)
{
    browser_nValue->setText(QString::number(nValue));
}

void View::addToSelectedDividers(int divider)
{
    browser_chosenDividers->append(QString::number(divider));
}

void View::addMovementToTheList(int player, int divider)
{
    QString text;
    if(player == 0)
        text = browser_firstPlayer->toPlainText() + " chose: " + QString::number(divider);
    else
        text = browser_secondPlayer->toPlainText() + " chose: " + QString::number(divider);

    browser_gameProgressList->append(text);
}

void View::setActivePlayer(int player)
{
    QPalette active, notActive;

    active.setColor(QPalette::Base, Qt::green);
    notActive.setColor(QPalette::Base, Qt::white);


    if(player == 0)
    {
        browser_firstPlayer->setPalette(active);
        browser_secondPlayer->setPalette(notActive);
    }
    else
    {
        browser_firstPlayer->setPalette(notActive);
        browser_secondPlayer->setPalette(active);
    }
}

void View::setPlayerNicks(QString firstPlayer, QString secondPlayer)
{
    browser_firstPlayer->setText(firstPlayer);
    browser_secondPlayer->setText(secondPlayer);
}

void View::refreshBrowsers()
{
    browser_gameProgressList->setText("");
    browser_actualNumberToDivide->setText("");
    browser_chosenDividers->setText("");
    browser_firstPlayer->setText("");
    browser_secondPlayer->setText("");
    browser_nValue->setText("");
}

QString View::getPlayerNickFromLineEdit()
{
    return lineEdit_playerNick->text();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------
// LABEL PROCEDURES AND FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------------------------

void View::warn(Warnings warning)
{
    if(warning == Warnings::clear)
        label_warnings->setText("");
    else if(warning == Warnings::dividerAlreadyExists)
        label_warnings->setText("Label already exists!");
    else if(warning == Warnings::tooManyDividers)
        label_warnings->setText("Too many dividers! The amount of them has been decreased.");
}

void View::showResults(int player)
{
    label_gameResults->show();
    if(player == 0)
        label_gameResults->setText(browser_firstPlayer->toPlainText() + " has won!");
    else
        label_gameResults->setText(browser_secondPlayer->toPlainText() + " has won!");
}

void View::hideResults()
{
    label_gameResults->hide();
}
