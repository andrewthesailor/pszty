#ifndef ENUMS_H
#define ENUMS_H

enum class GameTypes{OneCPU, TwoCPU};

enum class PlayerTypes{Human, Ai};

enum class Warnings{dividerAlreadyExists, tooManyDividers, clear};


#endif // ENUMS_H
